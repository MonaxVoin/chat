const ws = new WebSocket('ws://ws-chat-itclass.herokuapp.com');
const modaWrapper = document.querySelector('.wrapper');
const confirmBtn = document.querySelector('.confirm');
const userNameInput = document.querySelector('.user-name');
const messageInput = document.querySelector('.message-text');
const sendBtn = document.querySelector('.send');
const chatContainer = document.querySelector('.chat');


userNameInput.focus();

confirmBtn.addEventListener('click', setUserName);
userNameInput.addEventListener('keypress', (event) => enterKeyHander(event, setUserName));
sendBtn.addEventListener('click', sendMessage);
messageInput.addEventListener('keypress', (event) => enterKeyHander(event, sendMessage));

let userName;







function setUserName () {

  const inputValue = userNameInput.value;

  if(inputValue && inputValue.length >=2) {
    userName = inputValue;
    modaWrapper.style.display = 'none'
    messageInput.focus();
  } else {
    userNameInput.style.border = '1px solid red'
  }
}

function enterKeyHander(event, callback) {
  const keyCode = event.code;
  if(keyCode === 'Enter') {
    callback();
  }
}


function sendMessage() {
  const message = messageInput.value;
  if(message && message.length >0) {
    const messageObj = {
      name: userName,
      text: message,
      time: new Date(). getTime()
    };
    ws.send(JSON.stringify(messageObj));
    messageInput.value ='';
    messageInput.focus();
  }
}


function generateMessageTag(message) {
    const itemDiv = document.createElement('div');
    itemDiv.classList. add('item');
    if(message.name !== userName) {
      itemDiv.classList.add('item_alien');
    }

    const messageDiv = document.createElement('div');
    messageDiv.classList.add('message');

    const vendorDiv = document.createElement('div');
    vendorDiv.classList.add('vender');
    vendorDiv.innerText = message.name;



    const textDiv = document.createElement('div');
    textDiv.classList.add("text");
    textDiv.innerText = message.text;



    const timeDiv = document.createElement('div');
    timeDiv.classList.add('time');
    timeDiv.innerText = formattedTime(message.time);


    messageDiv.append(vendorDiv,textDiv, timeDiv);
    itemDiv.appendChild(messageDiv);
    return itemDiv;


}


function formattedTime (time) {
  const data = new Date();
  data.setTime(time);
  const hours = data.getHours();
  const minutes= data.getMinutes();
  return `${hours <  10 ? '0' + hours : hours}:${minutes <  10 ? '0' + minutes : minutes}`;
}






ws.onmessage = (message) => {
  // console.log(messageObj);
  const messageObg = JSON.parse(message.data);
  const messageTag = generateMessageTag(messageObg);
  chatContainer.appendChild(messageTag);
  messageTag.scrollIntoView({behavior:"smooth"});
}

// ws.onopen = () => {
//     ws.send('Privet');
// }



